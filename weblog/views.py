from django.db.models import Q
from django.utils import timezone
from django.views.generic.dates import (
    ArchiveIndexView, DateDetailView, DayArchiveView, MonthArchiveView,
    YearArchiveView
)

from core.models import Post
from weblog.models import Entry


class WeblogViewMixin():
    date_field = 'pub_date'
    paginated_by = 10
    slug_url_kwarg = 'entry_slug'

    def get_allow_future(self):
        return self.request.user.is_staff

    def get_queryset(self):
        if self.request.user.is_staff:
            return Entry.objects.all()
        else:
            return Entry.objects.filter(
                Q(status=Post.PUBLISHED_STATUS),
                Q(pub_date__date__lte=timezone.now())
            )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class WeblogDateDetailView(WeblogViewMixin, DateDetailView):
    pass


class WeblogDayArchiveView(WeblogViewMixin, DayArchiveView):
    pass


class WeblogMonthArchiveView(WeblogViewMixin, MonthArchiveView):
    pass


class WeblogYearArchiveView(WeblogViewMixin, YearArchiveView):
    make_object_list = True


class WeblogArchiveIndexView(WeblogViewMixin, ArchiveIndexView):
    pass
