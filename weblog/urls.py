from django.urls.conf import path

from weblog import views

app_name = 'weblog'
urlpatterns = [
    # https://docs.djangoproject.com/en/3.2/ref/class-based-views/mixins-date-based/
    path('<int:year>/<int:month>/<int:day>/<slug:entry_slug>/',
         views.WeblogDateDetailView.as_view(month_format='%m'),
         name="entry"),
    path('<int:year>/<int:month>/<int:day>/',
         views.WeblogDayArchiveView.as_view(month_format='%m'),
         name="archive-day"),
    path('<int:year>/<str:month>/',
         views.WeblogMonthArchiveView.as_view(month_format='%m'),
         name="archive-month"),
    path('<int:year>/',
         views.WeblogYearArchiveView.as_view(),
         name="archive-year"),
    path('',
         views.WeblogArchiveIndexView.as_view(),
         name="index"),
]
