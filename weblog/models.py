from django.db import models
from django.urls import reverse

from core.models import Post, Taxonomy, Tag


class Entry(Post):
    slug = models.SlugField(unique_for_date='pub_date')
    category = models.ForeignKey('Category', on_delete=models.PROTECT)

    class Meta():
        verbose_name_plural = 'Entries'
        # https://docs.djangoproject.com/en/3.2/ref/models/options/#ordering
        ordering = ('-pub_date',)

    def get_absolute_url(self):
        # https://docs.python.org/3/library/time.html#time.strftime
        kwargs = {
            'year': self.pub_date.year,
            'month': self.pub_date.strftime('%m').lower(),
            'day': self.pub_date.strftime('%d').lower(),
            'entry_slug': self.slug,
        }
        return reverse('weblog:entry', kwargs=kwargs)

    def __str__(self) -> str:
        return self.title


class Category(Taxonomy):
    class Meta():
        verbose_name_plural = 'Categories'

    def __str__(self) -> str:
        return self.title
