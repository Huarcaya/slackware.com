from typing import Any
from weblog.models import Category, Entry  #, Tag
from django.contrib import admin


class EntryAdmin(admin.ModelAdmin):
    exclude = ('author',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title',)
    list_display = ('title', 'category', 'creation_date', 'pub_date',
                    'last_modified', 'status', 'is_public', 'slug')
    list_filter = ('author', 'pub_date', 'status', 'category', 'tags')
    filter_horizontal = ('tags',)
    fieldsets = [
        ('Page info', {'fields': ['title', 'slug', 'content']}),
        ('Visibility', {'fields': ['is_public', 'status'],
                        'classes': ['collapse']}),
        ('Meta', {'fields': ['pub_date'],
                  'classes': ['collapse']}),
        ('Taxonomy', {'fields': ['category', 'tags'],
                      'classes': ['collapse']})
    ]

    def save_model(self, request: Any, obj: Any, form: Any, change: Any) -> None:
        obj.author = request.user
        super(EntryAdmin, self).save_model(request, obj, form, change)

    def post_url(self, obj) -> str:
        return obj.slug


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'slug')
    prepopulated_fields = {'slug': ('title',)}


# class TagAdmin(admin.ModelAdmin):
#     list_display = ('title', 'content', 'slug')
#     prepopulated_fields = {'slug': ('title',)}


admin.site.register(Entry, EntryAdmin)
admin.site.register(Category, CategoryAdmin)
# admin.site.register(Tag, TagAdmin)
