from django.db.models import Q
from django.shortcuts import redirect, render
from django.utils import timezone
from django.views.generic import DetailView, ListView

from core.models import Page, Post
from weblog.models import Entry

RECENTLY = 5


class IndexView(ListView):
    template_name = 'core/index.html'
    paginate_by = RECENTLY

    def get_queryset(self):
        return PostList.get_page_list()[:RECENTLY]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['latest_entry_list'] = Entry.objects.filter(
            Q(status=Post.PUBLISHED_STATUS),
            Q(pub_date__date__lte=timezone.now())
        )[:RECENTLY]
        context['latest_page_list'] = PostList.get_page_list()[:RECENTLY]
        return context


class PageDetailView(DetailView):
    """Page detail"""
    model = Page
    slug_url_kwarg = 'page_slug'
    context_object_name = 'page'

    def get(self, request, *args, **kwargs):
        page = PostList.get_page_list().get(slug=self.kwargs['page_slug'])

        if page.parent is not None:
            return redirect(f'/{page.parent.slug}/{page.slug}/')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class SubPageDetailView(DetailView):
    """Sub Page detail"""
    model = Page
    slug_url_kwarg = 'page_slug'

    def get(self, request, *args, **kwargs):
        page = PostList.get_page_list().get(
            slug=self.kwargs['page_slug'],
            parent__slug=self.kwargs['parent_slug'])
        return render(request, template_name='core/page_detail.html',
                      context={'page': page})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class PostList():
    """Post list"""

    def get_page_list():
        """Page list"""
        return Page.objects.filter(
            Q(status=Post.PUBLISHED_STATUS),
            Q(pub_date__date__lte=timezone.now())
        )
