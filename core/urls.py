# from django.conf import settings
# from django.conf.urls.static import static
from django.urls import path
from django.views.generic.base import RedirectView, TemplateView

from core import views

app_name = 'core'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),

    # Redirections
    path('docs/', RedirectView.as_view(url='https://docs.slackware.com/'),
         name='docs'),
    path('docs/beginners-guide/',
         RedirectView.as_view(
             url='https://docs.slackware.com/slackware:beginners_guide'),
         name='beginners-guide'),
    path('docs/installation-guide/',
         RedirectView.as_view(
             url='https://docs.slackware.com/slackware:install'),
         name='installation-guide'),
    path('download/', RedirectView.as_view(url='/get-slack/'),
         name='download'),
    path('favicon.ico', RedirectView.as_view(url='static/img/favicon.ico',
                                             permanent=True),
         name='favicon'),
    path('getslack/', RedirectView.as_view(url='/get-slack/'),
         name='getslack'),
    path('humans.txt', TemplateView.as_view(template_name="core/humans.txt",
                                            content_type='text/plain'),
         name='humans'),
    path('packages/',
         RedirectView.as_view(url='https://packages.slackware.com/'),
         name='packages'),
    path('planet/', RedirectView.as_view(url='https://planet.slackware.com'),
         name='planet'),
    path('slackbook/',
         RedirectView.as_view(url='https://www.slackbook.org/beta/'),
         name='slackbook'),

    # Page detail
    path('<slug:page_slug>/',
         views.PageDetailView.as_view(),
         name='page-detail'),
    # Sub Page detail
    path('<slug:parent_slug>/<slug:page_slug>/',
         views.SubPageDetailView.as_view(),
         name='subpage-detail'),
]
# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
