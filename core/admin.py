from core.models import Page
from django.contrib import admin

from .models import Page, Tag


class PageAdmin(admin.ModelAdmin):
    exclude = ('author',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('title',)
    list_display = ('title', 'creation_date', 'pub_date', 'last_modified',
                    'status', 'is_public', 'slug')
    list_filter = ('author', 'pub_date', 'status', 'tags')
    filter_horizontal = ('tags',)
    fieldsets = [
        ('Page info', {'fields': ['title', 'slug', 'content']}),
        ('Visibility', {'fields': ['is_public', 'status'],
                        'classes': ['collapse']}),
        ('Meta', {'fields': ['parent', 'pub_date'],
                  'classes': ['collapse']}),
        ('Taxonomy', {'fields': ['tags'],
                      'classes': ['collapse']})
    ]

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        super(PageAdmin, self).save_model(request, obj, form, change)

    def post_url(self, obj):
        return obj.slug


class TagAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'slug')
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Page, PageAdmin)
admin.site.register(Tag, TagAdmin)
