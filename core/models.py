from django.conf import settings
from django.db import models
from django.utils import timezone

import mistune
from pygments import highlight
from pygments.formatters import html
from pygments.lexers import get_lexer_by_name


class HighlightRenderer(mistune.Renderer):
    def block_code(self, code, lang):
        if not lang:
            return '\n<pre><code>%s</code></pre>\n' % mistune.escape(code)

        lexer = get_lexer_by_name(lang, stripall=True)
        formatter = html.HtmlFormatter()
        return highlight(code, lexer, formatter)


renderer = HighlightRenderer()
markdown = mistune.Markdown(renderer=renderer)


class Taxonomy(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    content = models.TextField()

    class Meta:
        abstract = True


class Tag(Taxonomy):
    def __str__(self):
        return self.title


class Post(models.Model):
    """Post abstract class"""

    # Post status
    PUBLISHED_STATUS = 'published'
    DRAFT_STATUS = 'draft'
    STATUS_CHOICES = (
        (PUBLISHED_STATUS.lower(), PUBLISHED_STATUS.title()),
        (DRAFT_STATUS.lower(), DRAFT_STATUS.title())
    )
    STATUS_CHOICES = sorted(STATUS_CHOICES)

    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.PROTECT)
    title = models.CharField(max_length=500)
    slug = models.SlugField(unique=True, max_length=200)
    content = models.TextField()
    content_html = models.TextField(editable=False)
    is_public = models.BooleanField(default=True)
    status = models.CharField(choices=STATUS_CHOICES, default=PUBLISHED_STATUS,
                              max_length=9)
    tags = models.ManyToManyField(Tag, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    pub_date = models.DateTimeField(default=timezone.now)
    last_modified = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.content_html = markdown(self.content)
        super().save(*args, **kwargs)

    class Meta:
        abstract = True
        # https://docs.djangoproject.com/en/3.2/ref/models/options/#ordering
        ordering = ('-pub_date',)


class Page(Post):
    """Page model"""
    parent = models.ForeignKey('Page', on_delete=models.SET_NULL,
                               limit_choices_to={'parent': None},
                               blank=True, null=True)

    def __str__(self):
        return self.title
